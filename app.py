import cv2
import numpy as np
import numpy
import time
from awss3 import *
from facedetection import *
import json
from microsoftfaceapi3 import *
import threading
from socket import *
from api import *
import traceback
import ast
import datetime
import os
import select
from multiprocessing import Queue
import re
import sys
from deepgaze.haar_cascade import haarCascade
from deepgaze.face_landmark_detection import faceLandmarkDetection
import math
import csv
import imutils
import multiprocessing
import threading
from threading import Thread
from multiprocessing.pool import ThreadPool
import json
from datetime import datetime
from collections import Counter
#from imutils.video import WebcamVideoStream
from emotion_api import *
import boto3
from boto3.dynamodb.conditions import Key, Attr
from app_utils import *
from multiprocessing import Pool, Queue, Process, Value
import multiprocessing
from ctypes import *
from imutils.video import VideoStream
from imutils import face_utils

manager = multiprocessing.Manager()
appid = -1
loc_id = manager.list()

android_response = manager.dict()
imcounter = 0
count = 0


display = manager.list()

multiface_data = manager.list()
invoke  = bool()
hps_response = manager.list()


#If True enables the verbose mode
DEBUG = False



P3D_RIGHT_SIDE = numpy.float32([-100.0, -77.5, -5.0]) #0
P3D_GONION_RIGHT = numpy.float32([-110.0, -77.5, -85.0]) #4
P3D_MENTON = numpy.float32([0.0, 0.0, -122.7]) #8
P3D_GONION_LEFT = numpy.float32([-110.0, 77.5, -85.0]) #12
P3D_LEFT_SIDE = numpy.float32([-100.0, 77.5, -5.0]) #16
P3D_FRONTAL_BREADTH_RIGHT = numpy.float32([-20.0, -56.1, 10.0]) #17
P3D_FRONTAL_BREADTH_LEFT = numpy.float32([-20.0, 56.1, 10.0]) #26
P3D_SELLION = numpy.float32([0.0, 0.0, 0.0]) #27
P3D_NOSE = numpy.float32([21.1, 0.0, -48.0]) #30
P3D_SUB_NOSE = numpy.float32([5.0, 0.0, -52.0]) #33
P3D_RIGHT_EYE = numpy.float32([-20.0, -65.5,-5.0]) #36
P3D_RIGHT_TEAR = numpy.float32([-10.0, -40.5,-5.0]) #39
P3D_LEFT_TEAR = numpy.float32([-10.0, 40.5,-5.0]) #42
P3D_LEFT_EYE = numpy.float32([-20.0, 65.5,-5.0]) #45
#P3D_LIP_RIGHT = numpy.float32([-20.0, 65.5,-5.0]) #48
#P3D_LIP_LEFT = numpy.float32([-20.0, 65.5,-5.0]) #54
P3D_STOMION = numpy.float32([10.0, 0.0, -75.0]) #62

#The points to track
#These points are the ones used by PnP
# to estimate the 3D pose of the face
TRACKED_POINTS = (0, 4, 8, 12, 16, 17, 26, 27, 30, 33, 36, 39, 42, 45, 62)
ALL_POINTS = list(range(0,68)) #Used for debug only

position = manager.list()
position = sys.argv[2]
person = manager.list()
person = sys.argv[1]
distance = manager.list()
distance = sys.argv[3]

myfile = open('./hps-testing/average-testing.csv', 'a')
myfile3 = open('./hps-testing/landmarks-points.csv', 'a')
myfile2 = open('./hps-testing/2-sec-averages.csv','a')
myfile4 = open('./hps-testing/range-values-2sec.csv','a')
myfile2 = open('./hps-testing/2-sec-averages.csv','a')
myfile5 = open('./hps-testing/'+position+'.csv','a')
myfile6 = open('./hps-testing/final_engagement.csv','a')
t3 = csv.writer(myfile3, delimiter=',',lineterminator='\n')
t = csv.writer(myfile, delimiter=',',lineterminator='\n')
t1 = csv.writer(myfile2, delimiter=',',lineterminator='\n')
t4 = csv.writer(myfile4, delimiter=',',lineterminator='\n')
t5 = csv.writer(myfile5, delimiter=',',lineterminator='\n')
t6 = csv.writer(myfile6, delimiter=',',lineterminator='\n')
#t.writerow(["Person","position","distance","fps","seconds","engaged percentage","1 percentage","0.75 percentage","not engaged percentage","2sec engaged","2sec 1's percentage","2sec 0.75 percentage","2sec not engaged","normal average","normal average without 0","2sec weighted average","2sec old weighted average","2sec normal average","weighted average","old weighted average","weighted average without 0","overall normal average","overall normal average without 0","difference normal average","difference weighted average","difference overall normal average"])
#t1.writerow(["Position","2 sec weighted averages","2sec normal averages"])
#t1.writerow(["Position","2 sec weighted averages","2sec normal averages"])
#t5.writerow(["Person","position","distance","campaign id","location ID","url","viewers","engaged percentage","1 percentage","0.75 percentage","not engaged percentage","2sec engaged","2sec 1's percentage","2sec 0.75 percentage","2sec not engaged","normal average","normal average without 0","2sec weighted average","2sec old weighted average","2sec normal average","weighted average","old weighted average","weighted average without 0","overall normal average","overall normal average without 0","difference normal average","difference weighted average","difference overall normal average"])
#t6.writerow(["Person","position","distance","campaign id","location ID","url","viewers",'gender','emotion_scores','emotion_average',"normal average","normal average without 0","2sec weighted average","2sec old weighted average","2sec normal average","weighted average","old weighted average","weighted average without 0","overall normal average","overall normal average without 0"])

weights = manager.list()
weights = [1,0.75,0.001]
weight2 = manager.list()
weight2 = [10,7.5,0.01]
display = manager.list()
results = manager.list()
r = manager.list()
inv = Value(c_bool, True)
counter = Value(c_bool, True)
now = manager.list()
now = datetime.now()
loc_id = Value(c_int,0)
lock = multiprocessing.Lock()
emotion_images = manager.list()
lock1 = threading.Lock()


def tcpConnection():
    global android_response
    global lock
    global loc_id
    global hps_response
    global inv
    
    serverPort = 49002
    #lock = multiprocessing.Lock()

    serverSocket = socket(AF_INET,SOCK_STREAM)
    serverSocket.bind(('',serverPort))
    serverSocket.listen(1)
    
    print ('The server is ready to receive')
    while 1:
        
        
        connectionSocket, addr = serverSocket.accept()
        sentence1 = connectionSocket.recv(1024)
        if len(android_response) > 0:
            print("greater response")
         
        sentence = sentence1.decode("utf-8")
        #print (sentence)
        lock.acquire()
        
        if(not (sentence[:6] == "app_id")):
            if(len(android_response) == 0):
                connectionSocket.send("wait".encode())
                
                
        
            else:
                print ("sending to android")
                print (str(json.dumps(android_response[0])))
                
                
                connectionSocket.send(json.dumps(android_response[0]).encode())
                inv.value = True
                #print(inv.value)
                hps_response.append(android_response[0])
                
                del android_response[0]
                
        else:
            appid = sentence[7:].strip()
            #print (appid)
            loc_id.value = int(appid)
            inv.value = False
            print(loc_id.value)
        lock.release()
        
        connectionSocket.close()
#Thread(target=tcpConnection, args=()).start()






def start_stream(input_q1,input_q):
    usingPiCamera = True
    #vs = manager.list()
    frameSize = (640, 480)
    vs = VideoStream(src=0, usePiCamera=True, resolution=frameSize,
		    framerate=10).start()
    time.sleep(0.1)
    global lock
    while 1:
        frame = vs.read()
        if frame is not None:
            #lock.acquire()
            if input_q1.qsize() <= 5:
                input_q1.put(frame)
            #if input_q1.qsize() > 40:
                #lock.acquire()
                #while input_q1.qsize() != 20:
                #    input_q1.get()
                #lock.release()
           
            #input_q1.put(frame)
           
            if input_q.qsize() < 20:
                input_q.put(frame)
            else:
                lock.acquire()
                while input_q.qsize() != 0:
                    input_q.get()
                lock.release()
                input_q.put(frame)
            #lock.release()
                
	    
            
                
            #print(input_q.qsize(),input_q1.qsize())
            time.sleep(0.01)

def cropFace(faces,frame):
    name = './emotion_images/'+"person_0"+str(datetime.now())+".jpg"
    image = frame[faces[1] -20:faces[3]+20, faces[0] -20 :faces[2]+20]
    image = imutils.resize(image, width=400)
    cv2.imwrite(name,image)
    return name

def calculate_engagement(yaw_r,pitch_r,hot_y,hot_p,results_q,frame):
    global results
    global r
    global position

    global now
    
    #name = './emotion-images/'+str(datetime.now())+".jpg"
    data = results_q.get()
    ya = data[0]
    pi = data[1]
    face_n = data[2]
    try:
        #print(ya,pi)
        then = datetime.now()
        diff = then - now
        #diff2 = then - now_2
        #print(diff2.seconds)
        if diff.seconds <=2:
            if ya in yaw_r and pi in pitch_r:
                if ya in hot_y and pi in hot_p:
                    
                    r.append(weights[0])
                    print(r)
                else:
                    
                    r.append(weights[1])
                    print(r)

            else:
                r.append(weights[2])
                print(r)
                
        else:
            #print(results)
            if ya in yaw_r and pi in pitch_r:
                
                if ya in hot_y and pi in hot_p:
                    
                    name = cropFace(face_n,frame)
                    emotion_images.append(name)
                    r.append(weights[0])
                    print(r)
                else:
                    
                    name = cropFace(face_n,frame)
                    emotion_images.append(name)
                    r.append(weights[1])
                    print(r)

            else:
                r.append(weights[2])
                print(r)
            now = datetime.now()
            results.append(r)
            t4.writerow([position,r])
            myfile4.flush()
            r = list()
                
    except Exception as e:
        print(e)
        results.append(r)
            #print(results)
        r = list()



def calc_weightavg(results):
    global weights
    global weight2
    merge = results
    merg_counter = Counter(merge)
    per_0 = merg_counter[weights[2]]/len(merge) #percentage of 0's
    tot_0 = per_0*weight2[2]
    #print("total not engaged",per_0)
    per_1 = merg_counter[weights[0]]/len(merge) #percentage of 1's
    tot_1 = per_1*weight2[0]
    #print("total 1 frames",per_1)
    per_65 = merg_counter[weights[1]]/len(merge) #percentage of 0.65
    tot_65 = per_65*weight2[1]
    
    #print("total 0.75 frames",per_65)
    if per_1+per_65 > 0.4:
        total_w = tot_1+tot_0+tot_65
        weighted_avg = tot_1*weights[0]+tot_65*weights[1]+tot_0*weights[2]/total_w
        total_ww = per_0+per_1+per_65
        weighted_avg2 = weights[2]*per_0+weights[1]*per_65+weights[0]*per_1/total_ww
        no_avg_2 = sum(merge)/len(merge)
    else:
        weighted_avg = weights[2]
        weighted_avg2 = weights[2]
        no_avg_2 = weights[2]
    return weighted_avg,weighted_avg2,per_1+per_65,per_0,no_avg_2,per_1,per_65



def insert_item(table_name, item):
    session = boto3.session.Session()
    dynamodb = session.resource('dynamodb')
    table = dynamodb.Table(table_name)
    response = table.put_item(Item=item)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return True
    else:
        return False


def insert_dynamo(loc,url,ad_id,gender,age,emotion_avg,engagement_score):
    tableName = 'advertelligent_engagement'
    #table1 = dynamodb.Table(tableName)
    #gen = json.loads(viewers)
    #view = json.dumps(viewers[0])
    #v = json.loads(view)
    emotion_data = {"person 0":{"gender":gender[0],"age": int(round(float(age[0]))),"emotion_average":emotion_avg,"engagement_score":int(round(float(engagement_score)))}}

    engagement_parameters = {"ad_url": url,"ad_id":ad_id}
    engagement_parameters = dict(engagement_parameters, **emotion_data)

    
    required_hash_data = {
        "location_id": loc,
        "timestamp": str(datetime.now())
        }
    item = dict(required_hash_data, **engagement_parameters)
    insert = insert_item(tableName,item)
    return insert


def emotion_calc(person,position,distance,z,loc,url,viewers,emotion_images_2,results_avg):
    global display
    #try:
    if emotion_images_2:
        emotion_score = list()
        print(emotion_images_2)
        if len(emotion_images_2) >= 3:
            emot,gender,age = get_emotion(emotion_images_2[:3])
        elif len(emotion_images_2) < 3:
            emot,gender,age = get_emotion(emotion_images_2)
        print(emot)
        print(gender)
        if emot and gender and age:
            emotion_score = calc_emotion(emot)
            print(emotion_score)
            results_avg[2] = results_avg[2]/10
            results_avg[5] = results_avg[5]/10
            results_avg[7] = results_avg[7]/10
            if len(set(gender)) == 1:
                emotion_avg = sum(emotion_score)/len(emotion_score)

                emotion_per = (emotion_avg/10)*0.15
                #new_results = list(map(lambda x: emotion_per+x*0.85,results_avg ))
                new_results = results_avg
                if len(display) >= 1:
                    x = len(display)
                    while x!=0:
                        del display[x-1]
                        x = len(display)

                display.append({"engagement :":results_avg[1]*100})
                display.append({"happy(0 - 10) :" : emotion_avg})
                emotion_data = [person,position,distance,z,loc,url,viewers,gender,emotion_score,emotion_avg]
                new_results1 = emotion_data+new_results
                t6.writerow(new_results1)
                
                
                
                myfile6.flush()
                emotion_images_2 = []
                print("added----------------------------------->")
                print(insert_dynamo(loc,url,z,gender,age,int(round(float(emotion_avg))),int(round(float(new_results[3]*100)))))
                #auth = insert_dynamo(loc,url,z,viewers,int(round(float(emotion_avg))),int(round(float(new_results[3]*100))))
                #print(auth)
    #except:
    #    print("emotion exception")




def score(results,z,url,viewers,loc,emotion_images):
    global weights
    global weight2
    global position
    global person
    global distance

    
    avg = list()
    avg_n = list()
    eng_2 = list()
    n_eng_2 = list()
    avg_2 = list() 
    avg_22 = list()
    no_avg_2 = list()
    pr_1 = list()
    pr_65 = list()
    try:
        if results:
            for r in results:
                if len(r) > 0:
                    wt_av,wt_av2,en,n_en,n_av,p_1,p_65=calc_weightavg(r)
                    avg_2.append(wt_av)
                    avg_22.append(wt_av2)
                    eng_2.append(en)
                    n_eng_2.append(n_en)
                    no_avg_2.append(n_av)
                    pr_1.append(p_1)
                    pr_65.append(p_65)
                    a = sum(r)/len(r)
                    avg_r = [f for f in r if f != weights[2]]
                    if avg_r:
                        b = sum(avg_r)/len(avg_r)
                        avg_n.append(b)
                    else:
                        avg_n.append(weights[2])
                    #aav = aav+a
                    avg.append(a)
            if pr_1:
                per_1_2 = sum(pr_1)/len(pr_1)
            else:
                per_1_2 = 0
            if pr_65:
                per_65_2 = sum(pr_65)/len(pr_65)
            else:
                per_65_2 = 0
            if  avg_22:
                average_22 = sum(avg_22)/len(avg_22)
            else:
                average_22 = 0
            if avg_2:
                average_2 = sum(avg_2)/len(avg_2)
            else:
                average_2 = 0
            if eng_2:
                avg_eng_2 = sum(eng_2)/len(eng_2)
            else:
                avg_eng_2 = 0
            if n_eng_2:
                n_avg_eng_2 = sum(n_eng_2)/len(n_eng_2)
            else:
                n_avg_eng_2 = 0
            if no_avg_2:
                normal_2 = sum(no_avg_2)/len(no_avg_2)
            else:
                normal_2 = 0
            if avg:
                no_avg = sum(avg)/len(avg)
            else:
                no_avg = 0
            if avg_n:
                norm_avg = sum(avg_n)/len(avg_n)
            else:
                norm_avg = 0

            if any(isinstance(el, list) for el in results):
                merge = sum([x for x in results],[])
            else:
                merge = results
            emotion_images_2 = emotion_images
            n_merge = [g for g in merge if g != weights[2]]
            merg_counter = Counter(merge)
            per_0 = merg_counter[weights[2]]/len(merge)
            tot_0 = per_0*weight2[2]
            #print("total not engaged",per_0)
            per_1 = merg_counter[weights[0]]/len(merge)
            tot_1 = per_1*weight2[0]
            #print("total 1 frames",per_1)
            per_65 = merg_counter[weights[1]]/len(merge)
            tot_65 = per_65*weight2[1]
            #print("total 0.75 frames",per_65)
            total_w = tot_1+tot_0+tot_65
            if len(n_merge) >0:
                n_merg_counter = Counter(n_merge)

                #n_per_0 = n_merg_counter[0]/len(n_merge)
                #n_tot_0 = n_merg_counter[0]*n_per_0
                #print("total not engaged",per_0)
                total_ww2 = per_0+per_1+per_65
                weighted_avg22 = weights[2]*per_0+weights[1]*per_65+weights[0]*per_1/total_ww2
                n_per_1 = n_merg_counter[weights[0]]/len(n_merge)
                n_tot_1 = n_per_1*weight2[0]
                #print("total 1 frames without 0",n_per_1)
                n_per_65 = n_merg_counter[weights[1]]/len(n_merge)
                n_tot_65 = n_per_65*weight2[1]
                #print("total 0.65 frames without 0",n_per_65)
                #print(len(merge))
                #print(len(n_merge))
                n_total_w = n_tot_1+n_tot_65
                n_avg = [x for x in merge if x != weights[2]]
                weighted_avg = tot_1*weights[0]+tot_65*weights[1]+tot_0*weights[2]/total_w
                n_weighted_avg = n_tot_1*weights[0]+n_tot_65*weights[1]/n_total_w
                

                #print("Normal Average with zeros",no_avg)
                
               
                #print(avr)
                #print(wtg)
                #avgs_arr = np.array(avr)
                #wt_arr = np.array(wtg)
                #print(avgs_arr)
                #print(wt_arr)
                normal = sum(merge)/len(merge)
                n_normal = sum(n_merge)/len(n_merge)
                
                #t1.writerow([position,avg_2,no_avg_2])
                #myfile2.flush()
                #t.writerow([person,position,distance,6,z,url,viewers,per_1+per_65,per_1,per_65,per_0,avg_eng_2,per_1_2,per_65_2,n_avg_eng_2,no_avg,norm_avg,average_2,average_22,normal_2,weighted_avg,weighted_avg22,n_weighted_avg,normal,n_normal,norm_avg-no_avg,n_weighted_avg-weighted_avg,n_normal-normal])
                #myfile.flush()
                results_avg = [no_avg,norm_avg,average_2,average_22,normal_2,weighted_avg,weighted_avg22,n_weighted_avg,normal,n_normal]
                #emot,gender,emotion_score = emotion_calc(emotion_score)
                t5.writerow([person,position,distance,z,loc,url,viewers,per_1+per_65,per_1,per_65,per_0,avg_eng_2,per_1_2,per_65_2,n_avg_eng_2,no_avg,norm_avg,average_2,average_22,normal_2,weighted_avg,weighted_avg22,n_weighted_avg,normal,n_normal,norm_avg-no_avg,n_weighted_avg-weighted_avg,n_normal-normal])
                myfile5.flush()
                #print(insert_dynamo(loc,url,z,viewers,results_avg[3]*100))
                print(results_avg[3]*100)
                Thread(target = emotion_calc , args= (person,position,distance,z,loc,url,viewers,emotion_images_2,results_avg)).start()
                emotion_images = []
        else:
            print("No results")
    except:
        print("average exception")

def main_hp(input_q1,output_q,cam_w,cam_h,camera_matrix,my_cascade,my_detector,landmarks_3D,camera_distortion):
    
    #Defining the video capture object
    
    #video_capture = cv2.VideoCapture("rtsp://192.168.1.4")
    
    
    #video_capture = vs.return_vd()
    #if(video_capture.isOpened() == False):
    #    print("Error: the resource is busy or unvailable")
    #else:
    #    print("The video source has been opened correctly...")

    #Create the main window and move it
    #cv2.namedWindow('Video')
    #cv2.moveWindow('Video', 20, 20)

    #Obtaining the CAM dimension
    

    #Defining the camera matrix.
    #To have better result it is necessary to find the focal
    # lenght of the camera. fx/fy are the focal lengths (in pixels) 
    # and cx/cy are the optical centres. These values can be obtained 
    # roughly by approximation, for example in a 640x480 camera:
    # cx = 640/2 = 320
    # cy = 480/2 = 240
    # fx = fy = cx/tan(60/2 * pi / 180) = 554.26
        #These are the camera matrix values estimated on my webcam with
    # the calibration code (see: src/calibration):
    #camera_matrix = numpy.float32([[602.10618226,          0.0, 320.27333589],
     #                              [         0.0, 603.55869786,  229.7537026], 
     #                              [         0.0,          0.0,          1.0] ])

    #Distortion coefficients
    #camera_distortion = numpy.float32([0.0, 0.0, 0.0, 0.0, 0.0])

    #Distortion coefficients estimated by calibration
    

    #Error counter definition
    no_face_counter = 0
    
    
    #Variables that identify the face
    #position in the main frame.
    face_x1 = 0
    face_y1 = 0
    face_x2 = 0
    face_y2 = 0
    face_w = 0
    face_h = 0

    #Variables that identify the ROI
    #position in the main frame.
    roi_x1 = 0
    roi_y1 = 0
    roi_x2 = cam_w
    roi_y2 = cam_h
    roi_w = cam_w
    roi_h = cam_h
    roi_resize_w = int(cam_w/10)
    roi_resize_h = int(cam_h/10)
    count_e = 0
    
    #ya = list()
    #pi = list()
    #count_ne = 0
    global hps_response
    global display
    DEBUG = True
    global inv
    yaw_r = range(10,60)
    pitch_r= range(10,90)
    hot_y = range(20,50)
    hot_p = range(20,80)
    #lock = multiprocessing.Lock()
    global lock

    while(True):
        
        #try:
        #video_capture = cv2.VideoCapture("rtsp://192.168.1.104")
        #frames = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
        #print(frames)
        #video_capture.set(cv2.CAP_PROP_POS_FRAMES,frames-1)
        

        
            #count_f = count_f.put([f])
        
        #if inv == False:
            
        #    print(f)
            
        #Create the main window and move it
        #cv2.namedWindow('Video')
        #cv2.moveWindow('Video', 20, 20)
        # Capture frame-by-frame
        
            #print(data.decode())
        #print(inv)
        
        #ret, frame = vs.read()
        #countFrame = countFrame + 1
        #print("what")
        if input_q1.qsize() > 1:
            lock.acquire()
            frame = input_q1.get()
            
            lock.release()
            #output_rgb = cv2.cvtColor(output_q.get(), cv2.COLOR_RGB2BGR)
            #frame = imutils.resize(frame, width=500)
            gray = cv2.cvtColor(frame[roi_y1:roi_y2, roi_x1:roi_x2], cv2.COLOR_BGR2GRAY)
            
            #frame = vs.read()
            #frame = imutils.resize(frame, width=500)
            #gray = cv2.cvtColor(frame[roi_y1:roi_y2, roi_x1:roi_x2], cv2.COLOR_BGR2GRAY)
        
            #print("no image")

            #Looking for faces with cascade
            #The classifier moves over the ROI
            #starting from a minimum dimension and augmentig
            #slightly based on the scale factor parameter.
            #The scale factor for the frontal face is 1.10 (10%)
            #Scale factor: 1.15=15%,1.25=25% ...ecc
            #Higher scale factors means faster classification
            #but lower accuracy.
            #
            #Return code: 1=Frontal, 2=FrontRotLeft, 
            # 3=FrontRotRight, 4=ProfileLeft, 5=ProfileRight.
            my_cascade.findFace(gray, True, True, True, True, 1.12, 1.2, 1.15, 1.15, 40, 40, rotationAngleCCW=30, rotationAngleCW=-30, lastFaceType=my_cascade.face_type)
            info_frame = frame
            


            #Accumulate error values in a counter
            if(my_cascade.face_type == 0): 
                no_face_counter += 1

            #If any face is found for a certain
            #number of cycles, then the ROI is reset
            if(no_face_counter == 50):
                no_face_counter = 0
                roi_x1 = 0
                roi_y1 = 0
                roi_x2 = cam_w
                roi_y2 = cam_h
                roi_w = cam_w
                roi_h = cam_h

            #Checking wich kind of face it is returned
            if(my_cascade.face_type > 0):

                #Face found, reset the error counter
                no_face_counter = 0

                #Because the dlib landmark detector wants a precise
                #boundary box of the face, it is necessary to resize
                #the box returned by the OpenCV haar detector.
                #Adjusting the frame for profile left
                if(my_cascade.face_type == 4):
                    face_margin_x1 = 20 - 10 #resize_rate + shift_rate
                    face_margin_y1 = 20 + 5 #resize_rate + shift_rate
                    face_margin_x2 = -20 - 10 #resize_rate + shift_rate
                    face_margin_y2 = -20 + 5 #resize_rate + shift_rate
                    face_margin_h = -0.7 #resize_factor
                    face_margin_w = -0.7 #resize_factor
                #Adjusting the frame for profile right
                elif(my_cascade.face_type == 5):
                    face_margin_x1 = 20 + 10
                    face_margin_y1 = 20 + 5
                    face_margin_x2 = -20 + 10
                    face_margin_y2 = -20 + 5
                    face_margin_h = -0.7
                    face_margin_w = -0.7
                #No adjustments
                else:
                    face_margin_x1 = 0
                    face_margin_y1 = 0
                    face_margin_x2 = 0
                    face_margin_y2 = 0
                    face_margin_h = 0
                    face_margin_w = 0

                #Updating the face position
                face_x1 = my_cascade.face_x + roi_x1 + face_margin_x1
                face_y1 = my_cascade.face_y + roi_y1 + face_margin_y1
                face_x2 = my_cascade.face_x + my_cascade.face_w + roi_x1 + face_margin_x2
                face_y2 = my_cascade.face_y + my_cascade.face_h + roi_y1 + face_margin_y2
                face_w = my_cascade.face_w + int(my_cascade.face_w * face_margin_w)
                face_h = my_cascade.face_h + int(my_cascade.face_h * face_margin_h)

                #Updating the ROI position       
                roi_x1 = face_x1 - roi_resize_w
                if (roi_x1 < 0): roi_x1 = 0
                roi_y1 = face_y1 - roi_resize_h
                if(roi_y1 < 0): roi_y1 = 0
                roi_w = face_w + roi_resize_w + roi_resize_w
                if(roi_w > cam_w): roi_w = cam_w
                roi_h = face_h + roi_resize_h + roi_resize_h
                if(roi_h > cam_h): roi_h = cam_h    
                roi_x2 = face_x2 + roi_resize_w
                if (roi_x2 > cam_w): roi_x2 = cam_w
                roi_y2 = face_y2 + roi_resize_h
                if(roi_y2 > cam_h): roi_y2 = cam_h
                

                #Debugging printing utilities
                if(DEBUG == True):
                    #print("FACE: ", face_x1, face_y1, face_x2, face_y2, face_w, face_h)
                    #print("ROI: ", roi_x1, roi_y1, roi_x2, roi_y2, roi_w, roi_h)
                    #Drawing a green rectangle
                    # (and text) around the face.
                    text_x1 = face_x1
                    text_y1 = face_y2 + 20
                    if(text_y1 < 0): text_y1 = 0
                    
                    cv2.rectangle(info_frame, 
                                 (face_x1, face_y1), 
                                 (face_x2, face_y2), 
                                 (0, 255, 0),
                                  2)

                    #cv2.putText(info_frame, "FACE", (text_x1,text_y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
                    if len(hps_response) >0:
                        d = hps_response[len(hps_response)-1]
                        view = d['viewers']
                        k = 20
                        for key,value in view[0].items():
                            cv2.putText(info_frame, key+" : "+str(value), (text_x1,text_y1+k), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
                            k = k+20
                        if display:
                            for x in display:
                                for key,value in x.items():
                                    cv2.putText(info_frame, key+" : "+str(value), (text_x1,text_y1+k), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 1);
                                    k = k+20

                #In case of a frontal/rotated face it
                # is called the landamark detector
                if(my_cascade.face_type > 0):
                    landmarks_2D = my_detector.returnLandmarks(info_frame, face_x1, face_y1, face_x2, face_y2, points_to_return=TRACKED_POINTS)

                    if(DEBUG == True):
                        #cv2.drawKeypoints(info_frame, landmarks_2D)

                        for point in landmarks_2D:
                    #        t3.writerow([point])
                            cv2.circle(frame,( point[0], point[1] ), 2, (0,0,255), -1)


                    #Applying the PnP solver to find the 3D pose
                    # of the head from the 2D position of the
                    # landmarks.
                    #retval - bool
                    #rvec - Output rotation vector that, together with tvec, brings 
                    # points from the model coordinate system to the camera coordinate system.
                    #tvec - Output translation vector.
                    retval, rvec, tvec = cv2.solvePnP(landmarks_3D, 
                                                      landmarks_2D, 
                                                      camera_matrix, camera_distortion)




                    R = cv2.Rodrigues(rvec)[0]
                    #rotation_value =  math.sqrt(abs(rotation_vector[0]*rotation_vector[0]) + abs(rotation_vector[1]*rotation_vector[0]) + abs(rotation_vector[2]*rotation_vector[2])) * 100
                    position_camera = -np.matrix(R).T * np.matrix(tvec)
                    cos_beta = math.sqrt(R[2,1] * R[2,1] + R[2,2] * R[2,2])
                    validity = cos_beta < 1e-6
                    if not validity:
                        alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                        beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                        gamma = math.atan2(R[2,1], R[2,2])    # roll  [x]
                    else:
                        alpha = math.atan2(R[1,0], R[0,0])    # yaw   [z]
                        beta  = math.atan2(-R[2,0], cos_beta) # pitch [y]
                        gamma = 0      
                    #cv2.imwrite(str(rotation_value)+'.jpeg',image)
                    angles = list()
                    euler = np.array([alpha, beta, gamma])
                    for x in euler:
                        angles.append(math.degrees(x))

                    #angles_q.put(angles)
                    yaw = angles[0]
                    pitch = angles[1]
                    roll = angles[2]
                    face_n = [face_x1,face_y1,face_x2,face_y2]
                    #print(yaw,pitch)
                    #ya = int(round(float(yaw)))
                    #pi = int(round(float(pitch)))
                    #cal_eng(ya,pi,yaw_r,pitch_r,hot_y,hot_p)
                    #then2 = datetime.now()
                    #diff2 = then2 - now2
                    #print(diff2.seconds)
                                    #print(diff2)
                    #if diff2.seconds-1 == 28 and tracke == False:
                    #    tracke = True
                    #    score(results,20)
                    #if diff2.seconds-1 == 20 and tracke == True:
                    #    tracke = False
                    #    score(results,10)
                    #print(yaw,pitch)
                    #print(inv)
                    #print("here1")
                    if inv.value == True:
                    #print(data2)
                        print(yaw,pitch)
                        #print("here")
                        ya = int(round(float(yaw)))
                        pi = int(round(float(pitch)))
                        results_q.put([ya,pi,face_n])
                        
                        
                        #save_img(face_x1,face_y1,face_w,face_h,frame)
                        #find = re.findall("True",data2)
                        
                    
                        
                            #ya.append()
                                
                                
                                

                       
                        
                                
                                #print(math.degrees(euler[0]))
                                #print(yaw,pitch)
                                #ts = time.time()
                                #st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
                                #print(ts,st)
                            
                                                        


                    #if int(yaw) in r and int(pitch) in p and int(roll) in ro:
                    #    count_e = count_e + 1
                    #    t.writerow([angles[0],angles[1],angles[2],"engaged",time.time(),count_e])
                        
                    #else:
                        
                    #    t.writerow([angles[0],angles[1],angles[2],"not engaged",time.time()])
                    #print (position_camera)
                    #print(count)
                    #print(angles)

                    #Now we project the 3D points into the image plane
                    #Creating a 3-axis to be used as reference in the image.
                    axis = numpy.float32([[50,0,0], 
                                          [0,50,0], 
                                          [0,0,50]])
                    imgpts, jac = cv2.projectPoints(axis, rvec, tvec, camera_matrix, camera_distortion)

                    #Drawing the three axis on the image frame.
                    #The opencv colors are defined as BGR colors such as: 
                    # (a, b, c) >> Blue = a, Green = b and Red = c
                    #Our axis/color convention is X=R, Y=G, Z=B
                    #sellion_xy = (landmarks_2D[7][0], landmarks_2D[7][1])
                    #cv2.line(info_frame, sellion_xy, tuple(imgpts[1].ravel()), (0,255,0), 3) #GREEN
                    #cv2.line(info_frame, sellion_xy, tuple(imgpts[2].ravel()), (255,0,0), 3) #BLUE
                    #cv2.line(info_frame, sellion_xy, tuple(imgpts[0].ravel()), (0,0,255), 3) #RED

            #Drawing a yellow rectangle
            # (and text) around the ROI.
            #if(DEBUG == True):
            #    text_x1 = roi_x1
            #    text_y1 = roi_y1 - 3
                #if(text_y1 < 0): text_y1 = 0
                #cv2.putText(frame, "ROI", (text_x1,text_y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,255), 1);
                #cv2.rectangle(frame, 
                #             (roi_x1, roi_y1), 
                #             (roi_x2, roi_y2), 
                #             (0, 255, 255),
                #             2)

            #Showing the frame and waiting
            # for the exit command
            
            #cv2.imshow('Video', frame)
            #if cv2.waitKey(17) & 0xFF == ord('q'):
            #    break
            
                    #return [ya,pi,face_n],info_frame   
            
            output_q.put([frame,info_frame])

    #except:
            #print("fuckin issue")
        #    continue
   
            #Release the camera
            #video_capture.release()
            #continue




def run_engagement(input_q1,output_q,results_q):
    global f
    global inv
    #ya = list()
    #pi = list()
    #count_ne = 0
    global hps_response
    global loc_id
    global results
    global data2
    global emotion_images
    global display
    counter = 0
    #stream = WebcamVideoStream(src="rtsp://192.168.1.4").start()
    #(grabbed, frame) = stream.read()
    #print(grabbed)
    cam_w = 640
    cam_h = 480
    c_x = cam_w / 2
    c_y = cam_h / 2
    f_x = c_x / numpy.tan(60/2 * numpy.pi / 180)
    f_y = f_x

    #Estimated camera matrix values.
    camera_matrix = numpy.float32([[f_x, 0.0, c_x],
                                   [0.0, f_y, c_y], 
                                   [0.0, 0.0, 1.0] ])

    print("Estimated camera matrix: \n" + str(camera_matrix) + "\n")
    #pool2 = Processl(1, ad_playing, (stream,))
    #pool = Pool(1, main_hp, (input_q1,output_q,cam_w,cam_h,camera_matrix,results_q))
    # initialize the variable used to indicate if the thread should
    camera_distortion = numpy.float32([ 0.06232237, -0.41559805,  0.00125389, -0.00402566,  0.04879263])


    #This matrix contains the 3D points of the
    # 11 landmarks we want to find. It has been
    # obtained from antrophometric measurement
    # on the human head.
    landmarks_3D = numpy.float32([P3D_RIGHT_SIDE,
                                  P3D_GONION_RIGHT,
                                  P3D_MENTON,
                                  P3D_GONION_LEFT,
                                  P3D_LEFT_SIDE,
                                  P3D_FRONTAL_BREADTH_RIGHT,
                                  P3D_FRONTAL_BREADTH_LEFT,
                                  P3D_SELLION,
                                  P3D_NOSE,
                                  P3D_SUB_NOSE,
                                  P3D_RIGHT_EYE,
                                  P3D_RIGHT_TEAR,
                                  P3D_LEFT_TEAR,
                                  P3D_LEFT_EYE,
                                  P3D_STOMION])

    #Declaring the two classifiers
    my_cascade = haarCascade("../etc/xml/haarcascade_frontalface_alt.xml", "../etc/xml/haarcascade_profileface.xml")
    #TODO If missing, example file can be retrieved from http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2
    my_detector = faceLandmarkDetection('../etc/shape_predictor_68_face_landmarks.dat')
    pool = Pool(1, main_hp, (input_q1,output_q,cam_w,cam_h,camera_matrix,my_cascade,my_detector,landmarks_3D,camera_distortion,))
    # be stopped
    stopped = False
    response = []
    yaw_r = range(-80,100)
    pitch_r= range(10,90)
    hot_y = range(-40,90)
    hot_p = range(20,85)
    ids = int()
    # keep looping infinitely until the thread is stopped
    while True:
        # if the thread indicator variable is set, stop the thread
     
            
            dat = output_q.get()
            frame = dat[0]
            output_rgb = dat[1]
            # write the frame
            if inv.value == True:
                #async_result = pool.apply_async(main_hp, (input_q1,output_q,cam_w,cam_h,camera_matrix,my_cascade,my_detector,landmarks_3D,camera_distortion,))
                #result,output_rgb = async_result.get()
		
                while results_q.qsize() > 1:
                #if result and output_rgb:
                    calculate_engagement(yaw_r,pitch_r,hot_y,hot_p,results_q,frame)
                if len(hps_response) >0:
                    #print("hps")
                    d = hps_response[len(hps_response)-1]
                    print(len(d))
                    #print(loc_id)
                    videourl = d['videourl']
                    ids = d['id']
                    viewers = d['viewers']
                    #print(viewers)
            elif inv.value == False:

                #print("False")
                if len(hps_response) > 0 and ids != 99:
                    #print("inside response")
                    if loc_id.value > 0:
                        print("inside score")
                        print("computed --------------->")
                        score(results,ids,videourl,viewers,loc_id.value,emotion_images)
                        results = []
                        loc_id.value = 0
                        emotion_images = []
                        del hps_response[0]
                        while results_q.qsize() != 0 :
                            results_q.get()
                        
                    else:
                        
                        results = []
                        loc_id.value = 0
            cv2.imshow('frame', output_rgb)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            
                    


                

    # When everything done, release the capture
    pool.join()
    pool.terminate()
    #stream.stop()
    #cv2.destroyAllWindows()
    
   


def s3andFaceApi(imagename, imagepath):
    global multiface_data
    print (imagepath)
    imagename = uploadToS3(imagepath)
    response = get_facial_features('https://s3.ap-south-1.amazonaws.com/face-api-temp-storage/'+imagename)
    if response:
        multiface_data.append(json.loads(response.decode("utf-8")))
    else:
        multiface_data = []


def ad_playing(input_q):
    global android_response
    global multiface_data

    detection_started = False
    detection_time = -1
    gaze_detection = False
    gaze_time_start = time.time()
    server = Thread(target=tcpConnection, args=()).start()
    
    face_ids=[]
    faces = []
##    stream = WebcamVideoStream(src="rtsp://192.168.1.4").start()
    global lock
    global lock1
    while(1):
        try:
            if(not (len(android_response) == 0)):
                continue
            #vcap = WebcamVideoStream(src="rtsp://192.168.1.4").start()
            #frames = vcap.get(cv2.CAP_PROP_FRAME_COUNT)
            #vcap.set(cv2.CAP_PROP_POS_FRAMES,frames-1);

            #cv2.imshow('VIDEO', frame)
            #ret,frame = stream.read()
            lock.acquire()
            frame = input_q.get()
            lock.release()
            if frame is not None:
                

                #try:
                #    if(not (len(frame.shape) ==3)):

                #                continue
                #       except:
                #           traceback.print_exc()
                #           continue


                #print str(faces)
                people, detected_faces = detectFace(frame)

                if(people > 0):
                   
                    if(people == 1):
                        frame = detected_faces[0]
                        try:
                            val = head_pose_estimation(frame)
                            if(not detection_started):
                                
                                detection_started = True
                                detection_time = time.time()
                            
                            faces.append({'value': val, 'image':frame})
                        except:
                            traceback.print_exc()
                            continue

                        timedifference = time.time() - detection_time
                        print (len(faces))
                        print (timedifference)
                        if(len(faces) > 0 and detection_started):
                            if(timedifference > 1):
                                if (not gaze_detection):
                                    gaze_detection = True
                                    gaze_time_start = time.time()
                                
                                detection_started = False
                           
                                min = 9999999
                                min_index = 0
                                in_count = 0
                                ts = time.time()
                                timestamp = datetime.now()
                                if(not os.path.isdir('images_with_info/'+str(timestamp))):
                                    os.mkdir('images_with_info/'+str(timestamp))
                            
                                
                                imagename =randstring()+ '.jpeg'
                                imagepath = 'images/'+imagename
                                
                                
                                timebasedpath = 'images_with_info/'+str(timestamp)+'/';

                                
                            
                            
                                cv2.imwrite(timebasedpath + 'selected.jpeg', frame)
                                cv2.imwrite(imagepath,frame)
                                imagename = uploadToS3(imagepath)
                                response1 = get_facial_features('https://s3.ap-south-1.amazonaws.com/face-api-temp-storage/'+imagename)
                                response = response1.decode("utf-8")
                                if response:
                                    data = json.loads(str(response))
                                    if data:
                                        f = open(timebasedpath + '/selected.txt','w')
                                        f.write(response)
                                        f.close()
                    
                                        print ("Response")
                                        
                                        print (response)
                                        try:
                                            
                                            gender = str(data[0]['faceAttributes']['gender'])
                                            age = str(data[0]['faceAttributes']['age'])
                                            beard = str(data[0]['faceAttributes']['facialHair']['beard'])
                 
                                            age = float(age) - (10*(float(beard)) + 5)
                                            age = str(age)
                                            
                                            face_ids.append(data[0]['faceId'])
                                            
                                            if(len(face_ids) >= 2):
                                                
                                                if (is_same_person(face_ids[0], face_ids[1])):
                                                    faces = []
                                                    face_ids.pop(0)
                                                    continue
                                                else:
                                                    
                                                    total_gaze_time = time.time() - gaze_time_start
                                                    gaze_detection = False
                                                    gaze_time_start = time.time()
                                                    print ("Total Gaze Time")
                                                    print (total_gaze_time)
                                                    print ("APP ID = ")
                                                    print (appid)
                                                    addGazeTimeLog(appid, total_gaze_time)
                                                    faces = []
                                                    face_ids.pop(0)
                                            ad_url = getAdvertisementUrl('https://advertelligent.co/adurl',[{"age":age,"gender": gender}])
                                            #ad_url = getAdvertisementUrl('http://127.0.0.1:8000/adurl',[{"age":age,"gender": gender}])
                                            print (ad_url)
                                            if re.findall('videourl',ad_url) and re.findall('id',ad_url):
                                                lock1.acquire()

                                            
                                                android_response = json.loads(ad_url)

                                                lock1.release()
                     
                                        except:
                                            faces = []
                                            print ("Exception")
                                            traceback.print_exc()
                                            continue
                                    else:
                                        continue
                                else:
                                    faces = []
                    else:

                        for face in detected_faces:
                            imagename =randstring()+ '.jpeg'
                            imagepath = 'images/'+imagename

                            cv2.imwrite(imagepath,  face)
                            
                                
                            try:
                                Thread( target=s3andFaceApi, args=(imagename, imagepath,) ).start()

                            except:
                                traceback.print_exc()
                                continue
                    

                        while(not(len(detected_faces) == len(multiface_data))):
                            hold = 0

                        gender_final = ""
                        age_sum = 0.0

                        persondata = []
                        
                        print ("Response")
                        print (multiface_data)
                        if multiface_data:
                            for item in multiface_data:
                                try:
                                    if item:
                                        persondata.append({
                                                          "age":item[0]['faceAttributes']['age'],
                                                          "gender": item[0]['faceAttributes']['gender']
                                                          })
                                        gender = item[0]['faceAttributes']['gender']
                                        age = item[0]['faceAttributes']['age']
                                        beard = str(item[0]['faceAttributes']['facialHair']['beard'])

                                        
                                except:
                                    faces = []
                                    traceback.print_exc()
                                    continue


                            multiface_data = []

                        try:
                            if persondata:
                                ad_url1 = getAdvertisementUrl('https://advertelligent.co/adurl', persondata)

                #ad_url = getAdvertisementUrl('http://127.0.0.1:8000/adurl', persondata)
                                print (ad_url1)
                                if re.findall('videourl',ad_url1) and re.findall('id',ad_url1):
                                    lock.acquire()
                                    
                                    android_response = json.loads(ad_url1)
                                    lock.release()
                    
                        except:
                            traceback.print_exc()
                            continue
                else:
                    if (gaze_detection):
                        total_gaze_time = time.time() - gaze_time_start
                        gaze_detection = False
                        gaze_time_start = time.time()
                        print ("Total Gaze Time")
                        print (total_gaze_time)
                        addGazeTimeLog(appid, total_gaze_time)

                count = 0

                #cv2.waitKey(1)
                count = count +1
        except:
            traceback.print_exc()
            server.join()
            print ("aa gayi exception")
            exit()


def display1(output_q):
	
    while True:
        #frame = vs.read()
        #input_q1.put(frame)
        # if the thread indicator variable is set, stop the thread
        if output_q.qsize() > 1:
            #lock.acquire()
            output_rgb = output_q.get()
            #lock.release()
            cv2.imshow('frame', output_rgb)
            if cv2.waitKey(1) & 0xFF == ord('q'):break
            #rawCapture.truncate(0)
    
    # When everything done, release the capture
    cv2.destroyAllWindows()
	

if __name__ == "__main__":
    input_q1 = Queue(maxsize=200)
    input_q = Queue(maxsize=100)
    output_q = Queue(maxsize=100)
    results_q = Queue(maxsize=100)
    loc_q = Queue(maxsize=1)
    
    


    #p = multiprocessing.Process(target=main, args=(input_q1,output_q,cam_w,cam_h,camera_matrix,results_q,))
    

   
    
    #pool.start()
    #input_q = Queue(maxsize=50)
    
    
    #now = datetime.now()
    #now_2 = datetime.now()
    #connection = "rtsp://192.168.1.4"
    #tcp_server = Thread(target=tcpConnection, args=())
    #tcp_server.start()
    
    

    #p = multiprocessing.Process(target=main, args=(input_q,output_q,cam_w,cam_h,camera_matrix,))
    #p.start()
    #pool2 = Processl(1, ad_playing, (stream,))
    get_f = Process(target=start_stream, args=(input_q1,input_q,))
    get_f.start()
    #pool = Pool(1,main,(input_q1,output_q,cam_w,cam_h,camera_matrix,results_q,))

    
    start_ad_playing = Process(target = ad_playing, args=(input_q,))
    run_engage = Process(target = run_engagement,args=(input_q1,output_q,results_q,))
    #p.daemon = True
    #p.start()
    #run_engage.daemon = True
    run_engage.start()
    #start_ad_playing.daemon = True
    start_ad_playing.start()
    #display = Process(target= display, args=(output_q,))
    #display.daemon = True
    #display.start()
    #pool.start()
    

    
    
    #p.join()
    run_engage.join()
    #get_f.join()
    start_ad_playing.join()
    #display.join()
    #pool.join()
    #tcp_server.terminate()

    #head_pose = Process(target=main, args=(stream,input_q1,output_q,cam_w,cam_h,camera_matrix,results_q))
    #head_pose.start()

    


    



