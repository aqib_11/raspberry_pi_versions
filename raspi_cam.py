import cv2
from imutils.video import VideoStream
from imutils import face_utils
import numpy as np
import time
usingPiCamera = True
#vs = manager.list()
frameSize = (320, 240)
vs = VideoStream(src=0, usePiCamera=usingPiCamera, resolution=frameSize,framerate=10).start()
time.sleep(1)
while True:

    frame = vs.read()
    #return frame
    cv2.imshow("video",frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):break
            #rawCapture.truncate(0)
    
    # When everything done, release the capture
cv2.destroyAllWindows()
